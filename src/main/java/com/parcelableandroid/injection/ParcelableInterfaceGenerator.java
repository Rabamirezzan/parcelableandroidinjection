package com.parcelableandroid.injection;

import android.os.Parcel;
import android.os.Parcelable;
import com.codeinjection.tools.*;
import com.codeinjection.tools.log.Log;
import com.sun.codemodel.*;

import javax.lang.model.element.TypeElement;
import javax.lang.model.type.TypeMirror;
import java.util.Iterator;

/**
 * Created by rramirezb on 02/01/2015.
 */
public class ParcelableInterfaceGenerator {
    public static final String PARCELABLE_CLASS_NAME = "android.os.Parcelable";
    public static final String PARCEL_CLASS_NAME = "android.os.Parcel";

    public static void generate(JDefinedClass modelClass, TypeElement modelInterface, ImplementationClassesReferences implementationReferences, JCodeModel codeModel) {

        TypeMirror superClass = modelInterface.getSuperclass();
        if (ElementTool.hasInterface(modelInterface, Parcelable.class)) {
            generateImplementation(modelClass, implementationReferences, codeModel);
        }
    }





    private static void generateReadFromParcel(JDefinedClass modelClass, JFieldVar creator, ImplementationClassesReferences implementationReferences, JCodeModel codeModel) {
        JMethod readFromParcel = modelClass.method(JMod.PUBLIC, void.class, "readFromParcel");
        JVar in = readFromParcel.param(codeModel.ref(PARCEL_CLASS_NAME), "in");

        Iterator<JFieldVar> fields = modelClass.fields().values().iterator();
        while (fields.hasNext()) {


            JFieldVar field = fields.next();


            setReadFromParcelMethodCall(readFromParcel.body(), field, in, creator, implementationReferences, codeModel);

        }

    }

    private static void generateWriteToParcel(JDefinedClass modelClass, ImplementationClassesReferences implementationReferences, JCodeModel codeModel) {
        JMethod writeToParcel = modelClass.method(JMod.PUBLIC, void.class, "writeToParcel");
        JVar out = writeToParcel.param(codeModel.ref(PARCEL_CLASS_NAME), "out");
        JVar flags = writeToParcel.param(int.class, "flags");
        JBlock body = writeToParcel.body();
        Iterator<JFieldVar> fields = modelClass.fields().values().iterator();
        while (fields.hasNext()) {
            JFieldVar field = fields.next();
            JInvocation call = setWriteToParcelMethodCall(field, out, flags, implementationReferences, codeModel);
            if (call != null) {
                body.add(call);
            }
        }


    }

    private static JInvocation setWriteToParcelMethodCall(JFieldVar field, JVar parcelOut, JVar flags, ImplementationClassesReferences implementationReferences, JCodeModel codeModel) {

        JInvocation call = null;
        JClass type = codeModel.ref(CodeModelTool.getTypeWithoutTypeParameters(field.type().fullName()));
        if (type.name().equals("int") || type.name().equals("Integer")) {
            call = parcelOut.invoke("writeInt").arg(JExpr.refthis(field.name()));
        } else if (type.name().equals("float") || type.name().equals("Float")) {
            call = parcelOut.invoke("writeFloat").arg(JExpr.refthis(field.name()));
        } else if (type.name().equals("double") || type.name().equals("Double")) {
            call = parcelOut.invoke("writeDouble").arg(JExpr.refthis(field.name()));
        } else if (type.name().equals("long") || type.name().equals("Long")) {
            call = parcelOut.invoke("writeLong").arg(JExpr.refthis(field.name()));
        } else if (type.name().equals("String")) {
            call = parcelOut.invoke("writeString").arg(JExpr.refthis(field.name()));
        } else if (ValidDataTypes.isList(field.type().fullName())) {
            String[] tps = CodeModelTool.getTypeParametersAsString(field.type().fullName());

            if (tps.length == 1) {
                JClass tp = codeModel.ref(tps[0]);
               // JClass refClass = codeModel.ref(Tool.getImplementedName(Tool.getImplementationPackage(modelData), tp.name(), modelData.getAnnotation()));
                boolean isParcelable = CodeModelTool.implementsInterface(tp, Parcelable.class);


                try {
                    //refClass.staticRef("CREATOR");
                    call = parcelOut.invoke("writeTypedList").arg(field);

                } catch (Exception e) {

                    e.printStackTrace();
                }
//
            }
        } else if (CodeModelTool.implementsInterface(type, Parcelable.class)) {
            call = parcelOut.invoke("writeParcelable").arg(JExpr.refthis(field.name())).arg(flags);
        }

        return call;

    }

    private static JInvocation setReadFromParcelMethodCall(JBlock body, JFieldVar field, JVar parcelIn, JFieldVar creator, ImplementationClassesReferences implementationReferences, JCodeModel codeModel) {

        JInvocation call = null;
        JClass type = codeModel.ref(CodeModelTool.getTypeWithoutTypeParameters(field.type().fullName()));
        if (type.name().equals("int") || type.name().equals("Integer")) {
            body.assign(JExpr.refthis(field.name()), parcelIn.invoke("readInt"));
        } else if (type.name().equals("float") || type.name().equals("Float")) {
            body.assign(JExpr.refthis(field.name()), parcelIn.invoke("readFloat"));
        } else if (type.name().equals("double") || type.name().equals("Double")) {
            body.assign(JExpr.refthis(field.name()), parcelIn.invoke("readDouble"));
        } else if (type.name().equals("long") || type.name().equals("Long")) {
            body.assign(JExpr.refthis(field.name()), parcelIn.invoke("readLong"));
        } else if (type.name().equals("String")) {
            body.assign(JExpr.refthis(field.name()), parcelIn.invoke("readString"));
        } else if (ValidDataTypes.isList(field.type().fullName())) {
            String[] tps = CodeModelTool.getTypeParametersAsString(field.type().fullName());
            if (tps.length == 1) {
                try {
                    JClass tp = codeModel.ref(tps[0]);

                    JClass refClass = codeModel.ref( implementationReferences.getImplementation(tp.fullName()));
                    refClass.staticRef("CREATOR");
                    JInvocation readList = parcelIn.invoke("readTypedList").arg(field).arg(refClass.staticRef("CREATOR"));
                    body.add(readList);
                } catch (Exception e) {

                    e.printStackTrace();
                }
            }
        } else if (CodeModelTool.implementsInterface(type, Parcelable.class)) {
            body.assign(JExpr.refthis(field.name()), parcelIn.invoke("readParcelable").arg(JExpr._this().invoke("getClass").invoke("getClassLoader")));
        }

        return call;

    }

    


    public static void generateImplementation(JDefinedClass modelClass, ImplementationClassesReferences implementationReferences, JCodeModel codeModel) {
        //writeToParcel(Parcel dest, int flags) method

        try {
            JFieldVar creator = generateCREATOR(modelClass, codeModel);

            generateParcelableConstructor(modelClass, codeModel);


            generateWriteToParcel(modelClass, implementationReferences, codeModel);
            generateReadFromParcel(modelClass, creator, implementationReferences, codeModel);

            //public abstract int describeContents () method

            JMethod describeContents = modelClass.method(JMod.PUBLIC, int.class, "describeContents");

            describeContents.body()._return(JExpr.direct("0"));
        } catch (Exception e) {

            Log.e( "[PARCELABLE]", "exception", ThrowableTool.getStackTrace(e));
            e.printStackTrace();
        }
        //public abstract int readFromParcel (Parcel in) method


    }



    public static void generateParcelableConstructor(JDefinedClass modelClass, JCodeModel codeModel) {
        JMethod c = modelClass.constructor(JMod.PUBLIC);
        JVar in = c.param(Parcel.class, "in");
        c.body().invoke("readFromParcel").arg(in);
    }

    public static JFieldVar generateCREATOR(JDefinedClass modelClass, JCodeModel codeModel) {
        JClass creatorClass = codeModel.ref(Parcelable.Creator.class).narrow(modelClass._implements().next());
        JDefinedClass anonymousClass = codeModel.anonymousClass(creatorClass);

        JInvocation init = JExpr._new(anonymousClass);
        JFieldVar creator = modelClass.field(JMod.PUBLIC | JMod.STATIC | JMod.FINAL, creatorClass, "CREATOR", init);


        JMethod createFromParcel = anonymousClass.method(JMod.PUBLIC, modelClass, "createFromParcel");
        JVar inParam = createFromParcel.param(Parcel.class, "in");
        createFromParcel.body()._return(JExpr._new(modelClass).arg(inParam));

        JMethod newArray = anonymousClass.method(JMod.PUBLIC, modelClass.array(), "newArray");
        JVar sizeParam = newArray.param(int.class, "size");

        JArray exp = JExpr.newArray(modelClass, sizeParam);
        newArray.body()._return(exp);

        return creator;
//        public static final Parcelable.Creator<MyParcelable> CREATOR
//                = new Parcelable.Creator<MyParcelable>() {
//            public MyParcelable createFromParcel(Parcel in) {
//                return new MyParcelable(in);
//            }
//
//            public MyParcelable[] newArray(int size) {
//                return new MyParcelable[size];
//            }
//        };
    }
}
